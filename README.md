# Moses

Web based solution to cut the price for taxi rides.
Calculates ride price and cut it between multiple groups of requestors.

Application is a web based solution for distance calculation and price cutting for taxi rides.

**User functions:**
Register
Login
Request new ride or join to existing ride
Start ride (the one he created). Otherwise ride will be started automatically, once the vehicle free seats are exceeded.

#Features
Price depends on setting values in database (price per mile and price per minute). Also it depends on type of vehicle (configurable in setting table)
If distance between 2 destinations less than 500m (by default) - drop off points will be merged (new destination will not be added to route)
Out of all currently available rides - you will get the one with the shortest distance.
You can edit user balance, manage price, maximum time delay, change vehicle type price with admin endpoints (not documented yet)

#API
To send requests to most of endpoints, you need to add Token to header, which you will get from POST /login response.
Header: 
key = Authorization value = {Token from /login endpoint}
**POST /register Not authorized**
input json
`{"username":"testadmin", "password":"test"}`

return
status 201

**POST /login Not authorized**
input form-data
`username=testadmin&password=test`

return
Authorization token

**GET /login Authorized**
No parameters

return object of currently logged in user
example
     `{"id": 1,
    "username": "testadmin",
    "balance": 0.0}`
    
**GET /ride Not Authorized**
input form-data
Returns list of suitable rides in your pick up location 
'origin= % Pick up destination %'
'passengers= % Amount of passengers with you %'

return object of currently logged in user
example
     `[
        {
        "id": 1,
        "duration": 781,
        "distance": 8044,
        "waypoints": [],
        "origin": "Raisy Okipnoi 8, Kiev",
        "destination": "Teodora Draizera St, 1, Kiev",
        "price": null
        }
    ]`
    
**POST /ride Authrorized**
Request new ride
if there are already created suitable rides for you, you will be joined there
if not - you will be the owner of a new ride

example request json
	{"destination":"Teodora Draizera St, 17, Kiev", 
	"passengersAmount":"1",
	"origin":"Teodora Draizera St, 1, Kiev",
	"vehicleType":"2"}
	
origin - your pick up location	
destination - your drop off location
passengersAmount - how many passengers are with you
vehicleType (optional) - which vehicle type do you need (1 (economy), 2 (bus), 3 (business) are currently supported)

example response json
```
{
    "id": 29,
    "duration": 250,
    "distance": 1606,
    "waypoints": [],
    "origin": "Teodora Draizera St, 1, Kiev",
    "destination": "Teodora Draizera St, 17, Kiev",
    "price": null
}
```


Http status 
204 - No available vehicles found at the moment. Try to remove 'vehicleType' to expand search.
500 Or 503 - Server error. Please contact admin.
404 - Route not found. Try to change or clarify origin address
200 - Everything's fine.

**POST /ride/start Authorized**
Start your ride, even if vehicle is not yet full.
example request json
	`{"id":"29"}`
	id - id of a ride
	
response json
	```
{
    "id": 29,
    "duration": 250,
    "distance": 1606,
    "waypoints": [],
    "origin": "Teodora Draizera St, 1, Kiev",
    "destination": "Teodora Draizera St, 17, Kiev",
    "price": 1.8
}
```
As a response you will receive your ride with calculated total price.




    





